import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game } from './game';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private apiUrl = 'https://api.rawg.io/api/';
  constructor(private httpClient: HttpClient) { }

  list(page?: string) {
    if (page) {
      return this.httpClient.get<GameList>(page);
    }
    return this.httpClient.get<GameList>(`${this.apiUrl}games?dates=2019-01-01,2019-12-31&ordering=-added`);
  }

  get(id) {
    return this.httpClient.get<Game>(`${this.apiUrl}games/${id}`);
  }
}

export interface GameList {
  count: number;
  next: string;
  previous: string;
  results: Array<Game>;
}
