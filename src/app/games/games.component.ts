import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';
import { Game } from '../game';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  constructor(private gameService: GameService) { }
  games: Game[];
  nextPage: string;
  previousPage: string;

  onChangePage(page): void {
    this.gameService.list(page)
      .subscribe(data => {
        console.log(data);
        this.games = data.results;
        this.nextPage = data.next;
        this.previousPage = data.previous;
      });
  }

  getGames(): void {
    this.gameService.list()
      .subscribe(data => {
        console.log(data);
        this.games = data.results;
        this.nextPage = data.next;
        this.previousPage = data.previous;
      });
  }

  ngOnInit() {
    this.getGames();
  }

}
