import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../game';
import { ActivatedRoute } from '@angular/router';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {
  game: Game;
  constructor(private route: ActivatedRoute,
    private gameService: GameService) {
  }

  getGame(id): void {
    this.gameService.get(id)
      .subscribe(data => {
        console.log(data);
        this.game = data;
      });
  }

  ngOnInit() {
    this.getGame(this.route.snapshot.paramMap.get('id'));
  }

}
