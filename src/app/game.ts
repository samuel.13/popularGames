export interface Game {
  id: number;
  name: string;
  background_image: string;
  genres: [];
  description: string;
  added: number;
}
